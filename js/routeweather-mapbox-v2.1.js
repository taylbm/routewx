var RTWX = {
    "shsr-visible": true,
    "shsr-opacity": 0.85,
    "animation-delay": 200,
    "animation-duration": 0,
    "total-frames": 15,
    "interval-id": null,
    "current-frame-name": null,
    "times-endpoint": "https://api.routewx.com/times",
    "date-slider-init": false,
    "frames": []
}

// Use the async library, to hit the times endpoint, 
// returning the availables times as unix timestamps

const getAvailableTimes = async () => {
    const response = await fetch(RTWX["times-endpoint"]) // get list of available times
    const times = await response.json() // parse JSON

    const totalFrames = RTWX["total-frames"]
    const timestamps = times["unixTimestamps"]
    RTWX["frames"] = timestamps
   
    let firstFrameDate = new Date(timestamps[timestamps.length - 1 - totalFrames])
    let lastFrameDate = new Date(timestamps[timestamps.length - 1])

    $("#date-range-slider").dateRangeSlider({
        bounds: {
            min: firstFrameDate,
            max: lastFrameDate
        },
        step: {
            minutes: 4
        },
        formatter: function(val){
	    d = new Date(val)
            return d.toLocaleString()
        }
    });
    RTWX["date-slider-init"] = true
}
function showRasterLayer(frameName) {
    if (RTWX["shsr-visible"]) {
        var duration = RTWX["animation-duration"]
        RTWX["current-frame-name"] = frameName
        var opacity = RTWX["shsr-opacity"]
        map.setPaintProperty(frameName, 'raster-opacity-transition', {duration:duration, delay: duration})
        map.setPaintProperty(frameName, 'raster-opacity', opacity)
    }
}
function hideRasterLayer(frameName) {
    var duration = RTWX["animation-duration"]
    map.setPaintProperty(frameName, 'raster-opacity-transition', {duration:duration, delay: duration})
    map.setPaintProperty(frameName, 'raster-opacity', 0)
}
function pauseAnimation() {
    clearInterval(RTWX["interval-id"])
}
function visibleClick(checkbox) {
    RTWX[checkbox.id] = checkbox.checked
}
function getFrameName(frameDate){
    let frameDateStr = frameDate.toISOString().replaceAll("-", "").split("T")[0]
    let frameHour = frameDate.toISOString().split("T")[1].split(":")[0]
    let frameMinute = frameDate.toISOString().split("T")[1].split(":")[1]
    return "shsr-" + frameDateStr + "-" + frameHour + frameMinute
}
function getFrameUrl(frameDate) {
    let frameName = getFrameName(frameDate)
    return "https://api.routewx.com/services/" + frameName 
}

function playAnimation() {
    var currentFrame = 0;
    var delay = RTWX["animation-delay"]
    const totalFrames = RTWX["total-frames"]

    var layers = map.getStyle().layers
    for (let [key, value] of Object.entries(layers)) {
        if (value.id.includes("shsr")) {
            map.removeLayer(value.id)
            map.removeSource(value.id)
        }
    }

    if (RTWX["date-slider-init"]) {
        //$("#date-range-slider").dateRangeSlider("destroy");
    }

    getAvailableTimes()

    RTWX["interval-id"] = setInterval(function () {
        var currentFrameMod = currentFrame % totalFrames
        let currentFrameIdx = RTWX["frames"].length - totalFrames + currentFrameMod - 1
        let currentFrameTimestamp = RTWX["frames"][currentFrameIdx]
	    let currentFrameDate = new Date(currentFrameTimestamp)
        let currentFrameName = getFrameName(currentFrameDate)
	    let currentFrameUrl = getFrameUrl(currentFrameDate)
        var shsrLayer = map.getLayer(currentFrameName)
        document.getElementById("datetime-display").innerHTML = currentFrameDate
        if (currentFrame > 0) {
            let previousFrameIdx = currentFrameMod == 0 ? RTWX["frames"].length - 2 : RTWX["frames"].length - totalFrames + currentFrameMod - 2
            let previousFrameTimestamp = RTWX["frames"][previousFrameIdx]
	        let previousFrameDate = new Date(previousFrameTimestamp)
            let previousFrameName = getFrameName(previousFrameDate)
	        let previousFrameUrl = getFrameUrl(previousFrameDate)

            $("#date-range-slider").dateRangeSlider("values", previousFrameDate, currentFrameDate);

            hideRasterLayer(previousFrameName)
        }
        if(typeof shsrLayer == 'undefined') {
            map.addSource(currentFrameName, {
                "type": "raster",
                "scheme": "xyz",
                "tiles": [currentFrameUrl + '/tiles/{z}/{x}/{y}.png'],
                "tileSize": 256,
                "attribution": "Seamless Hybrid-Scan Reflectivity (https://vlab.noaa.gov/web/wdtd/-/seamless-hybrid-scan-reflectivity-shsr-)"
            });

            map.addLayer({
                "id": currentFrameName,
                "type": "raster",
                "source": currentFrameName,
                "paint": {
                    "raster-opacity": RTWX["shsr-opacity"]
                }
            });
        }

        showRasterLayer(currentFrameName)
        currentFrame += 1
    }, delay);
}


document.getElementById('shsr-opacity-slider').oninput = function() {
    const val = document.getElementById('shsr-opacity-slider').value
    document.getElementById('shsr-slider-value').innerHTML = val
    RTWX["shsr-opacity"] = parseFloat(val)
};
document.getElementById('animation-delay-slider').oninput = function() {
    const val = document.getElementById('animation-delay-slider').value
    document.getElementById('animation-delay-value').innerHTML = val + " ms"
    RTWX["animation-delay"] = parseInt(val)
    clearInterval(RTWX["interval-id"])
    playAnimation()
};
document.getElementById('total-frames-slider').oninput = function() {
    const val = document.getElementById('total-frames-slider').value
    document.getElementById('total-frames-value').innerHTML = val
    RTWX["total-frames"] = parseInt(val)
    clearInterval(RTWX["interval-id"])
    playAnimation()
};


mapboxgl.accessToken = 'pk.eyJ1IjoibWV0c3RhdGluYyIsImEiOiJja3E5c2p0aW4wMmdlMnJuaGV2OG5yb3RtIn0.cCFZ8mJ12BPtviFQeeajbA';
var mapStyle = {
    'version': 8,
    'name': 'Dark',
    'sources': {
        'mapbox': {
            'type': 'vector',
            'url': 'mapbox://mapbox.mapbox-streets-v8'
        }
    },
    'sprite': 'mapbox://sprites/mapbox/dark-v10',
    'glyphs': 'mapbox://fonts/mapbox/{fontstack}/{range}.pbf',
    'layers': [
        {
            'id': 'background',
            'type': 'background',
            'paint': { 'background-color': '#111' }
        },
        {
            'id': 'water',
            'source': 'mapbox',
            'source-layer': 'water',
            'type': 'fill',
            'paint': { 'fill-color': '#2c2c2c' }
        },
        {
            'id': 'boundaries',
            'source': 'mapbox',
            'source-layer': 'admin',
            'type': 'line',
            'paint': {
                'line-color': '#797979',
                'line-dasharray': [2, 2, 6, 2]
            },
            'filter': ['all', ['==', 'maritime', 0]]
        },
        {
            'id': 'cities',
            'source': 'mapbox',
            'source-layer': 'place_label',
            'type': 'symbol',
            'layout': {
                'text-field': '{name_en}',
                'text-font': ['DIN Offc Pro Bold', 'Arial Unicode MS Bold'],
                'text-size': [
                    'interpolate',
                    ['linear'],
                    ['zoom'],
                    4,
                    9,
                    6,
                    12
                ]
            },
            'paint': {
                'text-color': '#969696',
                'text-halo-width': 2,
                'text-halo-color': 'rgba(0, 0, 0, 0.85)'
            }
        },
        {
            'id': 'states',
            'source': 'mapbox',
            'source-layer': 'place_label',
            'type': 'symbol',
            'layout': {
                'text-transform': 'uppercase',
                'text-field': '{name_en}',
                'text-font': ['DIN Offc Pro Bold', 'Arial Unicode MS Bold'],
                'text-letter-spacing': 0.15,
                'text-max-width': 7,
                'text-size': [
                    'interpolate',
                    ['linear'],
                    ['zoom'],
                    4,
                    10,
                    6,
                    14
                ]
            },
            'filter': ['==', ['get', 'class'], 'state'],
            'paint': {
                'text-color': '#969696',
                'text-halo-width': 2,
                'text-halo-color': 'rgba(0, 0, 0, 0.85)'
            }
        }
    ]
};

var map = new mapboxgl.Map({
    container: 'map',
    maxZoom: 7,
    minZoom: 3,
    zoom: 5,
    center: [-87, 37],
    style: mapStyle,
});


$("#date-range-slider").on("userValuesChanged", function(e, data){
    const frameName = getFrameName(data.values.max)
    hideRasterLayer(RTWX["current-frame-name"])
    document.getElementById("datetime-display").innerHTML = data.values.max
    showRasterLayer(frameName)
});

map.on('load', function () {
    getAvailableTimes()
    playAnimation()
    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            (position) => {
            const pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude,
            };
            map.setCenter(pos);
            },
            () => {
            handleLocationError(true, infoWindow, map.getCenter());
            }
        );
    } else {
        // Browser doesn't support Geolocation
    }
});

//$("#play-button").on("click touchend", playAnimation);
$("#animation-button").on("touchend click", function(event) {
    event.stopPropagation();
    event.preventDefault();
    if ($(this)[0].alt == "pause") {
        console.log($(this)[0].alt)
        pauseAnimation()
        $(this)[0].alt = "play"
        $(this)[0].src = document.location.origin + "/img/play-button.png"
    }
    else {
        playAnimation()
        $(this)[0].alt = "pause"
        $(this)[0].src = document.location.origin + "/img/pause-button.png"    
    }
});

setInterval(function () {
    getAvailableTimes()
}, 180000);
